const ClientsStorage = require('../ClientsStorage');
const tripcode = require('tripcode');
const Validators = require('../Validators/Validators');
const Commands = require('./Commands/Commands');
const { IgnoreChat } = require('./Commands/Ignores');
const { Configuration, OpenAIApi } = require("openai");

const configuration = new Configuration({
	apiKey: "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",
});

function ChatRoom(gameName) {
	let clients = new Set();
	let self = this;

	this.GetSocketByUUID = function(uuid) {
		for(let socket of clients) {
			if(socket.uuid == uuid)
				return socket;
		}
	}

	this.ClientsCount = function() {
		return clients.size;
	}

	this.SendFromSocketToUUID = function(socket, uuid, message) {
		let receiverSocket = self.GetSocketByUUID(uuid);
		if((Commands.bans.chat.includes[socket.uuid] && socket.uuid != socket.uuid) || ClientsStorage.IsClientIgnoredByClientInChat(socket, receiverSocket))
			return;
		receiverSocket.send(JSON.stringify(message));
	}

	function Broadcast(broadsocket, message, otherSocketsOnly) {

		for(let socket of clients) {
			if(
				!ClientsStorage.IsClientIgnoredByClientInChat(broadsocket, socket) && 
				!(otherSocketsOnly && socket.uuid == broadsocket.uuid) &&
				//when user is chat-banned we still want to send them their messages even if they'll join from another tab
				//that will prevent users from easily realising that they were banned
				!(Commands.bans.chat.includes(broadsocket.uuid) && broadsocket.uuid != socket.uuid) 
			)
				if(typeof message == 'object')
					socket.send(JSON.stringify(message));
				else
					socket.send(message);
		}
	}

	this.Disconnect = function(socket) 
	{	
		if(socket.name && socket.trip) {
			Broadcast(socket, {
				type: "roomDisconnect",
				name: socket.name,
				trip: socket.trip
			});
		}
		clients.delete(socket);
	}

	this.Connect = function(socket) {
		clients.add(socket);
		socket.chatRoom = self;
		socket.onmessage = async function(e) {
			if(e.data.length > 4096) {
				return;
			}

			let msgjson;
			try {
				msgjson = JSON.parse(e.data);
			} catch (err) {
				YNOnline.Network.logWarning({
					tags: ["invalid packets", "invalid json"],
					text: "invalid json when receiving chat message"
				}
				);
				return;			
			}

			if(msgjson.type == "pong")
				return;

			if(socket.name && socket.trip) {
				
				if(Array.isArray(msgjson.command)) {
					socket.send(JSON.stringify({type: "serverInfo", text: Commands.ExecuteCommand(socket, msgjson.command)}));
				}

				if(typeof msgjson.text === "string") {
					Broadcast(socket, JSON.stringify({type: "userMessage", text: msgjson.text, name: socket.name + "#" + socket.uuid, trip: socket.trip}));
					if (msgjson.text[0] == "?") {
						try {
							const openai = new OpenAIApi(configuration);
							const response = await openai.createChatCompletion({
								model: "gpt-3.5-turbo",
								messages: [
									//{"role": "system", "content": "You are a helpful assistant."},
									//{"role": "user", "content": "Who won the world series in 2020?"},
									//{"role": "assistant", "content": "The Los Angeles Dodgers won the World Series in 2020."},
									{"role": "user", "content": msgjson.text.substr(1)}
								]
							});							
							msgjson.text = response.data.choices[0].message.content;	
							msgjson.text = msgjson.text.replace(/\s*/g,"");
							console.log(msgjson.text);
							Broadcast(socket, JSON.stringify({type: "userMessage", text: msgjson.text, name: socket.name + "#" + socket.uuid, trip: socket.trip}));
						} catch(error) {
							msgjson.text = "Error!"
							if (error.response) {
								console.error(error.response.status, error.response.data);
							} else {
								console.error(`Error with OpenAI API request: ${error.message}`);
							}
						}
					}					
				}
			}
			else {
				if(typeof msgjson.name !== "string" || typeof msgjson.trip !== "string") {
					YNOnline.Network.logWarning({
						tags: [
							"invalid packets",
							"invalid json"
						],
						text: "name or tripcode is not a string",
						extra: {
							socket: socket,
							packet: msgjson
						}
					});
					return;
				}
				else {

					if(!Validators.ValidateName(msgjson.name))
						return;

					socket.name = msgjson.name;
					socket.trip = tripcode(msgjson.trip);

					for(let s of ClientsStorage.SessionClients[socket.address].sockets) {
						if(s)
						s.trip = socket.trip;
					}

					Broadcast(socket, JSON.stringify({type: "userConnect", name: socket.name, trip: socket.trip}), true);
				}
			}
		}
	}
}

module.exports = ChatRoom;